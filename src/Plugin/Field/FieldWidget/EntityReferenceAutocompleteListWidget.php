<?php

namespace Drupal\autocomplete_list_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;

/**
 * Plugin implementation of the 'entity_reference_autocomplete_list' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_autocomplete_list",
 *   label = @Translation("Autocomplete (List style)"),
 *   description = @Translation("An autocomplete text field. Use this for multiple value references."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceAutocompleteListWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['match_operator'] = [
      '#type' => 'radios',
      '#title' => t('Autocomplete matching'),
      '#default_value' => $this->getSetting('match_operator'),
      '#options' => $this->getMatchOperatorOptions(),
      '#description' => t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of entities.'),
    ];
    $element['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#min' => 1,
      '#required' => TRUE,
    ];
    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $operators = $this->getMatchOperatorOptions();
    $summary[] = t('Autocomplete matching: @match_operator', ['@match_operator' => $operators[$this->getSetting('match_operator')]]);
    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    $placeholder = $this->getSetting('placeholder');
    if (!empty($placeholder)) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
    }
    else {
      $summary[] = t('No placeholder');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $referenced_entities = $items->referencedEntities();
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $values = $form_state->getValue($field_name);
    $entity = NULL;
    if ($values) {
      if ($id = (isset($values[$delta]['target_id']['target_id']) ? $values[$delta]['target_id']['target_id'] : NULL)) {
        $entity_type = $this->getFieldSetting('target_type');
        $entity = \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->load($id);
      }
    }
    elseif (isset($referenced_entities[$delta])) {
      $entity = $referenced_entities[$delta];
    }

    if ($entity) {
      $element += [
        'markup' => [
          '#type' => 'item',
          'link' => $entity->toLink()->toRenderable(),
          'remove' => [
            '#type' => 'submit',
            '#name' => 'remove_' . $delta,
            '#value' => t('Remove'),
            '#submit' => [[get_class($this), 'removeSubmit']],
            '#limit_validation_errors' => [array_merge($parents, [$field_name])],
            '#ajax' => [
              'callback' => [get_class($this), 'removeAjax'],
              'effect' => 'fade',
            ],
          ],
          '#prefix' => '<div class="element">',
          '#suffix' => '</div>',
        ],
        'target_id' => [
          '#type' => 'value',
          '#value' => $entity->id(),
        ],
      ];
    }
    return ['target_id' => $element];
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return $element['target_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      if (isset($value['target_id'])) {
        if (is_array($value['target_id'])) {
          unset($values[$key]['target_id']);
          if (isset($value['target_id']['target_id'])) {
            $values[$key]['target_id'] = $value['target_id']['target_id'];
          }
          else {
            $values[$key] += $value['target_id'];
          }
        }
      }
      else {
        unset($values[$key]);
      }
    }

    return $values;
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getMatchOperatorOptions() {
    return [
      'STARTS_WITH' => t('Starts with'),
      'CONTAINS' => t('Contains'),
    ];
  }

  /**
   * Special handling to create form elements for multiple values.
   *
   * Handles generic features for multiple fields:
   * - number of widgets
   * - AHAH-'add more' button
   * - table display and drag-n-drop value reordering
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();
    $parents = $form['#parents'];

    $id_prefix = implode('-', array_merge($parents, [$field_name]));
    $wrapper_id = Html::getUniqueId($id_prefix . '-add-more-wrapper');

    // Determine the number of widgets to display.
    switch ($cardinality) {
      case FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED:
        $field_state = static::getWidgetState($parents, $field_name, $form_state);
        $max = $field_state['items_count'];
        break;

      default:
        $max = $cardinality - 1;
        break;
    }
    $title = $this->fieldDefinition->getLabel();
    $description = FieldFilteredMarkup::create(\Drupal::token()
      ->replace($this->fieldDefinition->getDescription()));

    $selection_settings = $this->getFieldSetting('handler_settings') + ['match_operator' => $this->getSetting('match_operator')];

    $elements = [
      'add_new_element' => [
        '#type' => 'container',
        'autocomplete_input' => [
          '#title' => $this->fieldDefinition->getLabel(),
          '#description' => FieldFilteredMarkup::create(\Drupal::token()->replace($this->fieldDefinition->getDescription())),
          '#type' => 'entity_autocomplete',
          '#target_type' => $this->getFieldSetting('target_type'),
          '#selection_handler' => $this->getFieldSetting('handler'),
          '#selection_settings' => $selection_settings,
          // Entity reference field items are handling validation themselves via
          // the 'ValidReference' constraint.
          '#validate_reference' => FALSE,
          '#maxlength' => 1024,
          '#default_value' => NULL,
          '#size' => $this->getSetting('size'),
          '#placeholder' => $this->getSetting('placeholder'),
        ],
        'add' => [
          '#type' => 'submit',
          '#value' => t('Add this'),
          '#submit' => [[get_class($this), 'addMoreSubmit']],
          '#limit_validation_errors' => [array_merge($parents, [$field_name])],
          '#ajax' => [
            'callback' => [get_class($this), 'addMoreAjax'],
            'wrapper' => $wrapper_id,
            'effect' => 'fade',
          ],
        ],
      ],

    ];

    for ($delta = 0; $delta <= $max; $delta++) {
      // Add a new empty item if it doesn't exist yet at this delta.
      if (!isset($items[$delta])) {
        $items->appendItem();
      }

      $element = [];

      $element = $this->formSingleElement($items, $delta, $element, $form, $form_state);

      if ($element) {
        $element['target_id']['markup']['remove']['#ajax']['wrapper'] = $wrapper_id;

        $elements[$delta] = $element;
      }
    }

    if ($elements) {
      $elements += [
        '#field_name' => $field_name,
        '#entity_type' => $this->getFieldSetting('target_type'),
        '#cardinality' => $cardinality,
        '#cardinality_multiple' => $this->fieldDefinition->getFieldStorageDefinition()
          ->isMultiple(),
        '#required' => $this->fieldDefinition->isRequired(),
        '#title' => $title,
        '#description' => $description,
        '#max_delta' => $max,
      ];

      $elements['#prefix'] = '<div id="' . $wrapper_id . '">';
      $elements['#suffix'] = '</div>';
    }

    return $elements;
  }

  /**
   * Generates the form element for a single copy of the widget.
   */
  protected function formSingleElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#field_parents' => $form['#parents'],
      '#delta' => $delta,
      '#weight' => $delta,
    ];

    $element = $this->formElement($items, $delta, $element, $form, $form_state);

    if ($element) {
      // Allow modules to alter the field widget form element.
      $context = [
        'form' => $form,
        'widget' => $this,
        'items' => $items,
        'delta' => $delta,
        'default' => $this->isDefaultValueWidget($form_state),
      ];
      \Drupal::moduleHandler()->alter([
        'field_widget_form',
        'field_widget_' . $this->getPluginId() . '_form',
      ], $element, $form_state, $context);
    }

    return $element;
  }

  /**
   * Submission handler for the "Add another item" button.
   */
  public static function addMoreSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $parents = array_slice($button['#array_parents'], 0, -2);
    $element = NestedArray::getValue($form, $parents);

    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Increment the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['items_count']++;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    // Add new values to form_state.
    $delta = $element['#max_delta'];
    $values = $form_state->getValue($element['#field_name']);
    $id = $values['add_new_element']['autocomplete_input'];
    $values = array_merge($values, [$delta => ['target_id' => ['target_id' => $id]]]);
    $form_state->setValue($element['#field_name'], $values);

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    if ($key_exists) {
      // Account for drag-and-drop reordering if needed.
      if (!$this->handlesMultipleValues()) {
        // Remove the 'value' of the 'add more' button.
        unset($values['add_more']);
        unset($values['add_new_element']);
        // The original delta, before drag-and-drop reordering, is needed to
        // route errors to the correct form element.
        foreach ($values as $delta => &$value) {
          $value['_original_delta'] = $delta;
        }

      }

      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values and remove the empty ones.
      $items->setValue($values);
      $items->filterEmptyItems();

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      foreach ($items as $delta => $item) {
        $field_state['original_deltas'][$delta] = isset($item->_original_delta) ? $item->_original_delta : $delta;
        unset($item->_original_delta, $item->_weight);
      }
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }
  }

  /**
   * Ajax callback for the "Add this" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   */
  public static function addMoreAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));
    $form_state->setRebuild();
    return $element;
  }

  public static function removeSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $parents = array_slice($button['#array_parents'], 0, -4);
    $element = NestedArray::getValue($form, $parents);

    // Add new values to form_state.
    $delta = array_slice($button['#parents'], -4, 1);
    $delta = reset($delta);
    $values = $form_state->getValue($element['#field_name']);
    unset($values[$delta]);
    $form_state->setValue($element['#field_name'], $values);

    $form_state->setRebuild();
  }

  /**
   * Ajax callback for the "Remove" button.
   */
  public static function removeAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -5));
    return $element;
  }

}
